package main

import (
	"reflect"
	"testing"
)

func Test_invertTree(t *testing.T) {
	tests := []struct {
		name string
		root *TreeNode
		want []int
	}{
		{
			name: "empty tree",
			root: nil,
			want: []int{},
		},
		{
			name: "complete 2-level tree",
			root: &TreeNode{Val: 4,
				Left: &TreeNode{Val: 2,
					Left:  &TreeNode{Val: 1, Left: nil, Right: nil},
					Right: &TreeNode{Val: 3, Left: nil, Right: nil},
				},
				Right: &TreeNode{Val: 7,
					Left:  &TreeNode{Val: 6, Left: nil, Right: nil},
					Right: &TreeNode{Val: 9, Left: nil, Right: nil},
				},
			},
			want: []int{4, 7, 2, 9, 6, 3, 1},
		},
		{
			name: "complete 3-level tree",
			root: &TreeNode{Val: 4,
				Left: &TreeNode{Val: 2,
					Left: &TreeNode{Val: 1,
						Left:  &TreeNode{Val: 11, Left: nil, Right: nil},
						Right: &TreeNode{Val: 12, Left: nil, Right: nil},
					},
					Right: &TreeNode{Val: 3,
						Left:  &TreeNode{Val: 13, Left: nil, Right: nil},
						Right: &TreeNode{Val: 14, Left: nil, Right: nil},
					},
				},
				Right: &TreeNode{Val: 7,
					Left: &TreeNode{Val: 6,
						Left:  &TreeNode{Val: 15, Left: nil, Right: nil},
						Right: &TreeNode{Val: 16, Left: nil, Right: nil},
					},
					Right: &TreeNode{Val: 9,
						Left:  &TreeNode{Val: 17, Left: nil, Right: nil},
						Right: &TreeNode{Val: 18, Left: nil, Right: nil},
					},
				},
			},
			want: []int{4, 7, 2, 9, 6, 3, 1, 18, 17, 16, 15, 14, 13, 12, 11},
		},
		{
			name: "incomplete 2-level tree",
			root: &TreeNode{Val: 4,
				Left: &TreeNode{Val: 2,
					Left:  nil,
					Right: &TreeNode{Val: 3, Left: nil, Right: nil},
				},
				Right: &TreeNode{Val: 7,
					Left:  &TreeNode{Val: 6, Left: nil, Right: nil},
					Right: nil,
				},
			},
			want: []int{4, 7, 2, 6, 3},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("test panic recovered: %v", err)
				}
			}()

			got := invertTree(tt.root)
			if s := treeToSlice(got); !reflect.DeepEqual(s, tt.want) {
				t.Errorf("invertTree() = %v, want %v", s, tt.want)
			}
		})
	}
}

func treeToSlice(roots ...*TreeNode) []int {
	s := make([]int, 0)
	next := make([]*TreeNode, 0)
	for _, r := range roots {
		if r == nil {
			continue
		}
		s = append(s, r.Val)
		next = append(next, r.Left, r.Right)
	}
	if len(next) > 0 {
		s = append(s, treeToSlice(next...)...)
	}
	return s
}
