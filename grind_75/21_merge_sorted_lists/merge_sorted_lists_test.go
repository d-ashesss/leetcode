package main

import (
	"reflect"
	"testing"
)

func Test_mergeTwoLists(t *testing.T) {
	type args struct {
		list1 *ListNode
		list2 *ListNode
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "equal elements",
			args: args{
				list1: &ListNode{1, &ListNode{2, &ListNode{4, nil}}},
				list2: &ListNode{1, &ListNode{3, &ListNode{4, nil}}},
			},
			want: []int{1, 1, 2, 3, 4, 4},
		},
		{
			name: "first has more elements",
			args: args{
				list1: &ListNode{1, &ListNode{2, &ListNode{4, &ListNode{6, nil}}}},
				list2: &ListNode{1, &ListNode{3, &ListNode{4, nil}}},
			},
			want: []int{1, 1, 2, 3, 4, 4, 6},
		},
		{
			name: "second has more elements",
			args: args{
				list1: &ListNode{1, &ListNode{2, &ListNode{4, nil}}},
				list2: &ListNode{1, &ListNode{3, &ListNode{4, &ListNode{6, nil}}}},
			},
			want: []int{1, 1, 2, 3, 4, 4, 6},
		},
		{
			name: "lists empty",
			args: args{
				list1: nil,
				list2: nil,
			},
			want: []int{},
		},
		{
			name: "first empty",
			args: args{
				list1: nil,
				list2: &ListNode{0, nil},
			},
			want: []int{0},
		},
		{
			name: "second empty",
			args: args{
				list1: &ListNode{0, nil},
				list2: nil,
			},
			want: []int{0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("test panicked: %v", err)
				}
			}()

			got := mergeTwoLists(tt.args.list1, tt.args.list2)
			if s := listToSlice(got); !reflect.DeepEqual(s, tt.want) {
				t.Errorf("mergeTwoLists() = %v, want %v", got, tt.want)
			}
		})
	}
}

func listToSlice(l *ListNode) []int {
	s := make([]int, 0)
	for l != nil {
		s = append(s, l.Val)
		l = l.Next
	}
	return s
}
