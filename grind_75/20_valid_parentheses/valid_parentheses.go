package main

var parentheses = map[byte]byte{
	')': '(',
	'}': '{',
	']': '[',
}

func isValid(s string) bool {
	b := []byte(s)
	opens := make([]byte, 0)

	for _, ch := range b {
		pair, closing := parentheses[ch]
		if !closing {
			opens = append(opens, ch)
			continue
		}
		if len(opens) == 0 {
			return false
		}
		if opens[len(opens)-1] != pair {
			return false
		}
		opens = opens[:len(opens)-1]
	}
	if len(opens) > 0 {
		return false
	}
	return true
}
