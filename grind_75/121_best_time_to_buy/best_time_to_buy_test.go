package main

import "testing"

func Test_maxProfit(t *testing.T) {
	type args struct {
		prices []int
	}
	tests := []struct {
		name string
		args args
		want int
	}{
		{
			name: "empty list",
			args: args{prices: []int{}},
			want: 0,
		},
		{
			name: "complete list",
			args: args{prices: []int{7, 1, 5, 3, 6, 4}},
			want: 5,
		},
		{
			name: "no sell",
			args: args{prices: []int{7, 6, 4, 3, 1}},
			want: 0,
		},
		{
			name: "one day",
			args: args{prices: []int{7}},
			want: 0,
		},
		{
			name: "zero sell",
			args: args{prices: []int{7, 6, 4, 3, 3}},
			want: 0,
		},
		{
			name: "zero buy",
			args: args{prices: []int{7, 6, 0, 3, 4}},
			want: 4,
		},
		{
			name: "best buy in the middle",
			args: args{prices: []int{4, 2, 4, 1}},
			want: 2,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("test panicked: %v", err)
				}
			}()

			if got := maxProfit(tt.args.prices); got != tt.want {
				t.Errorf("maxProfit() = %v, want %v", got, tt.want)
			}
		})
	}
}
