package main

import "strings"

func isPalindrome(s string) bool {
	if len(s) < 2 {
		return true
	}
	b := []byte(strings.ToLower(s))
	i := 0
	j := len(b) - 1
	for {
		if !isAlphanum(b[i]) {
			i++
			if i >= len(b) {
				break
			}
			continue
		}
		for !isAlphanum(b[j]) {
			j--
		}
		if j <= i {
			break
		}
		if b[i] != b[j] {
			return false
		}
		i++
		j--
	}
	return true
}

func isAlphanum(b byte) bool {
	return ('0' <= b && b <= '9') || ('A' <= b && b <= 'Z') || ('a' <= b && b <= 'z')
}
