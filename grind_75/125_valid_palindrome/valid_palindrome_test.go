package main

import "testing"

func Test_isPalindrome(t *testing.T) {
	tests := []struct {
		s    string
		want bool
	}{
		{
			s:    "ab_a",
			want: true,
		},
		{
			s:    "",
			want: true,
		},
		{
			s:    " ",
			want: true,
		},
		{
			s:    "  ",
			want: true,
		},
		{
			s:    "  a  ",
			want: true,
		},
		{
			s:    "a",
			want: true,
		},
		{
			s:    "aa",
			want: true,
		},
		{
			s:    "aba",
			want: true,
		},
		{
			s:    "ab_a",
			want: true,
		},
		{
			s:    "A man, a plan, a canal: Panama",
			want: true,
		},
		{
			s:    "race a car",
			want: false,
		},
		{
			s:    "1A",
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(tt.s, func(t *testing.T) {
			defer func() {
				if err := recover(); err != nil {
					t.Fatalf("test paniced: %v", err)
				}
			}()

			if got := isPalindrome(tt.s); got != tt.want {
				t.Errorf("isPalindrome() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_isAlphanum(t *testing.T) {
	tests := []struct {
		b    byte
		want bool
	}{
		{
			b:    '0',
			want: true,
		},
		{
			b:    'A',
			want: true,
		},
		{
			b:    'Z',
			want: true,
		},
		{
			b:    'a',
			want: true,
		},
		{
			b:    'z',
			want: true,
		},
		{
			b:    '/',
			want: false,
		},
		{
			b:    '{',
			want: false,
		},
		{
			b:    ' ',
			want: false,
		},
		{
			b:    ':',
			want: false,
		},
		{
			b:    '_',
			want: false,
		},
	}
	for _, tt := range tests {
		t.Run(string(tt.b), func(t *testing.T) {
			if got := isAlphanum(tt.b); got != tt.want {
				t.Errorf("isAlphanum() = %v, want %v", got, tt.want)
			}
		})
	}
}
