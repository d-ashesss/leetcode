package recursion_1

func reverseString(s []byte) {
	if len(s) < 2 {
		return
	}
	right := len(s) - 1
	s[0], s[right] = s[right], s[0]
	reverseString(s[1:right])
}
