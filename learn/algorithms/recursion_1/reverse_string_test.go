package recursion_1

import (
	"bytes"
	"testing"
)

func Test_reverseString(t *testing.T) {
	tests := []struct {
		name string
		str  []byte
		want []byte
	}{
		{
			name: "empty string",
			str:  []byte(""),
			want: []byte(""),
		},
		{
			name: "one char",
			str:  []byte("h"),
			want: []byte("h"),
		},
		{
			name: "two chars",
			str:  []byte("ho"),
			want: []byte("oh"),
		},
		{
			name: "even chars",
			str:  []byte("hola"),
			want: []byte("aloh"),
		},
		{
			name: "odd chars",
			str:  []byte("hello"),
			want: []byte("olleh"),
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			reverseString(tt.str)
			if !bytes.Equal(tt.str, tt.want) {
				t.Errorf("got %q, want %q", tt.str, tt.want)
			}
		})
	}
}

func Benchmark_reverseString(b *testing.B) {
	for i := 0; i < b.N; i++ {
		reverseString([]byte("Lorem ipsum dolor sit amet, consectetur adipiscing elit."))
	}
}
