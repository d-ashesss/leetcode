package p739_daily_temperatures

import (
	"reflect"
	"testing"
)

func Test_dailyTemperaturesIteration(t *testing.T) {
	tests := []struct {
		name         string
		temperatures []int
		want         []int
	}{
		{
			name:         "[81 80 79 71 69 72 76 73 76 73]",
			temperatures: []int{81, 80, 79, 71, 69, 72, 76, 73, 76, 73},
			want:         []int{0, 0, 0, 2, 1, 1, 0, 1, 0, 0},
		},
		{
			name:         "[73 74 75 71 69 72 76 73]",
			temperatures: []int{73, 74, 75, 71, 69, 72, 76, 73},
			want:         []int{1, 1, 4, 2, 1, 1, 0, 0},
		},
		{
			name:         "[30 70 40 50 60]",
			temperatures: []int{30, 70, 40, 50, 60},
			want:         []int{1, 0, 1, 1, 0},
		},
		{
			name:         "[30 60 90]",
			temperatures: []int{30, 60, 90},
			want:         []int{1, 1, 0},
		},
		{
			name:         "[90 60 30]",
			temperatures: []int{90, 60, 30},
			want:         []int{0, 0, 0},
		},
		{
			name:         "[30]",
			temperatures: []int{30},
			want:         []int{0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := dailyTemperaturesIteration(tt.temperatures); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dailyTemperaturesIteration() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dailyTemperaturesMonotonicStack(t *testing.T) {
	tests := []struct {
		name         string
		temperatures []int
		want         []int
	}{
		{
			name:         "[81 80 79 71 69 72 76 73 76 73]",
			temperatures: []int{81, 80, 79, 71, 69, 72, 76, 73, 76, 73},
			want:         []int{0, 0, 0, 2, 1, 1, 0, 1, 0, 0},
		},
		{
			name:         "[73 74 75 71 69 72 76 73]",
			temperatures: []int{73, 74, 75, 71, 69, 72, 76, 73},
			want:         []int{1, 1, 4, 2, 1, 1, 0, 0},
		},
		{
			name:         "[30 70 40 50 60]",
			temperatures: []int{30, 70, 40, 50, 60},
			want:         []int{1, 0, 1, 1, 0},
		},
		{
			name:         "[30 60 90]",
			temperatures: []int{30, 60, 90},
			want:         []int{1, 1, 0},
		},
		{
			name:         "[90 60 30]",
			temperatures: []int{90, 60, 30},
			want:         []int{0, 0, 0},
		},
		{
			name:         "[30]",
			temperatures: []int{30},
			want:         []int{0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := dailyTemperaturesMonotonicStack(tt.temperatures); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dailyTemperaturesMonotonicStack() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_dailyTemperaturesReverse(t *testing.T) {
	tests := []struct {
		name         string
		temperatures []int
		want         []int
	}{
		{
			name:         "[81 80 79 71 69 72 76 73 76 73]",
			temperatures: []int{81, 80, 79, 71, 69, 72, 76, 73, 76, 73},
			want:         []int{0, 0, 0, 2, 1, 1, 0, 1, 0, 0},
		},
		{
			name:         "[73 74 75 71 69 72 76 73]",
			temperatures: []int{73, 74, 75, 71, 69, 72, 76, 73},
			want:         []int{1, 1, 4, 2, 1, 1, 0, 0},
		},
		{
			name:         "[30 70 40 50 60]",
			temperatures: []int{30, 70, 40, 50, 60},
			want:         []int{1, 0, 1, 1, 0},
		},
		{
			name:         "[30 60 90]",
			temperatures: []int{30, 60, 90},
			want:         []int{1, 1, 0},
		},
		{
			name:         "[90 60 30]",
			temperatures: []int{90, 60, 30},
			want:         []int{0, 0, 0},
		},
		{
			name:         "[30]",
			temperatures: []int{30},
			want:         []int{0},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := dailyTemperaturesReverse(tt.temperatures); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("dailyTemperaturesReverse() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_dailyTemperatures(b *testing.B) {
	b.Run("dailyTemperaturesIteration", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			dailyTemperaturesIteration([]int{81, 80, 79, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73})
		}
	})
	b.Run("dailyTemperaturesMonotonicStack", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			dailyTemperaturesMonotonicStack([]int{81, 80, 79, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73})
		}
	})
	b.Run("dailyTemperaturesReverse", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			dailyTemperaturesReverse([]int{81, 80, 79, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73, 74, 75, 71, 69, 72, 76, 73})
		}
	})
}
