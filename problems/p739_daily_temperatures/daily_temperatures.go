package p739_daily_temperatures

// Simple iteration approach
func dailyTemperaturesIteration(temperatures []int) []int {
	answer := make([]int, len(temperatures))
	for i, temp := range temperatures {
		for j := i + 1; j < len(temperatures); j++ {
			if temperatures[j] > temp {
				answer[i] = j - i
				break
			}
		}
	}
	return answer
}

// Caches values without match into monotonic stack
func dailyTemperaturesMonotonicStack(temperatures []int) []int {
	answer := make([]int, len(temperatures))
	lessers := make([]int, 0, len(temperatures))
	for i, temp := range temperatures {
		for les := len(lessers) - 1; les >= 0; les-- {
			j := lessers[les]
			if temp > temperatures[j] {
				answer[j] = i - j
				lessers = lessers[:les]
			} else {
				break
			}
		}
		lessers = append(lessers, i)
	}
	return answer
}

// Optimized iteration from the end
func dailyTemperaturesReverse(temperatures []int) []int {
	answer := make([]int, len(temperatures))
	max := 0
	for i := len(temperatures) - 1; i >= 0; i-- {
		temp := temperatures[i]
		if temp >= max {
			max = temp
			continue
		}
		days := 1
		for temperatures[i+days] <= temp {
			days += answer[i+days]
		}
		answer[i] = days
	}
	return answer
}
