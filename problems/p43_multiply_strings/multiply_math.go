package p43_multiply_strings

import (
	"strings"
)

// Simple math multiplication solution
func multiplyMath(num1 string, num2 string) string {
	switch {
	case num1 == "0" || num2 == "0":
		return "0"
	case num1 == "1":
		return num2
	case num2 == "1":
		return num1
	}
	buf := make([]byte, len(num1)+len(num2))
	for i := len(num1) - 1; i >= 0; i-- {
		n1 := num1[i] - '0'
		for j := len(num2) - 1; j >= 0; j-- {
			n2 := num2[j] - '0'
			p := n1*n2 + buf[i+j+1]
			buf[i+j+1] = p % 10
			buf[i+j] += p / 10
		}
	}
	for i := range buf {
		buf[i] += '0'
	}
	return strings.TrimLeft(string(buf), "0")
}
