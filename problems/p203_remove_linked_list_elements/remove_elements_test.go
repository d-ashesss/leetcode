package p203_remove_linked_list_elements

import (
	"reflect"
	"testing"
)

func listToSlice(head *ListNode) []int {
	if head == nil {
		return nil
	}
	return append([]int{head.Val}, listToSlice(head.Next)...)
}

func Test_removeElementsRecursiveImmutable(t *testing.T) {
	type args struct {
		head *ListNode
		val  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "remove 6",
			args: args{
				head: &ListNode{
					Val: 1,
					Next: &ListNode{
						Val: 2,
						Next: &ListNode{
							Val: 6,
							Next: &ListNode{
								Val: 3,
								Next: &ListNode{
									Val: 4,
									Next: &ListNode{
										Val: 5,
										Next: &ListNode{
											Val:  6,
											Next: nil,
										},
									},
								},
							},
						},
					},
				},
				val: 6,
			},
			want: []int{1, 2, 3, 4, 5},
		},
		{
			name: "empty input",
			args: args{
				head: nil,
				val:  6,
			},
			want: nil,
		},
		{
			name: "remove all",
			args: args{
				head: &ListNode{
					Val: 7,
					Next: &ListNode{
						Val: 7,
						Next: &ListNode{
							Val:  7,
							Next: nil,
						},
					},
				},
				val: 7,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotList := removeElementsRecursiveImmutable(tt.args.head, tt.args.val)
			got := listToSlice(gotList)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("removeElementsRecursiveImmutable() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Test_removeElementsRecursiveMutable(t *testing.T) {
	type args struct {
		head *ListNode
		val  int
	}
	tests := []struct {
		name string
		args args
		want []int
	}{
		{
			name: "remove 6",
			args: args{
				head: &ListNode{
					Val: 1,
					Next: &ListNode{
						Val: 2,
						Next: &ListNode{
							Val: 6,
							Next: &ListNode{
								Val: 3,
								Next: &ListNode{
									Val: 4,
									Next: &ListNode{
										Val: 5,
										Next: &ListNode{
											Val:  6,
											Next: nil,
										},
									},
								},
							},
						},
					},
				},
				val: 6,
			},
			want: []int{1, 2, 3, 4, 5},
		},
		{
			name: "empty input",
			args: args{
				head: nil,
				val:  6,
			},
			want: nil,
		},
		{
			name: "remove all",
			args: args{
				head: &ListNode{
					Val: 7,
					Next: &ListNode{
						Val: 7,
						Next: &ListNode{
							Val:  7,
							Next: nil,
						},
					},
				},
				val: 7,
			},
			want: nil,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			gotList := removeElementsRecursiveMutable(tt.args.head, tt.args.val)
			got := listToSlice(gotList)
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("removeElementsRecursiveMutable() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_removeElementsRecursiveImmutable(b *testing.B) {
	for i := 0; i < b.N; i++ {
		removeElementsRecursiveImmutable(&ListNode{
			Val: 1,
			Next: &ListNode{
				Val: 2,
				Next: &ListNode{
					Val: 6,
					Next: &ListNode{
						Val: 3,
						Next: &ListNode{
							Val: 4,
							Next: &ListNode{
								Val: 5,
								Next: &ListNode{
									Val:  6,
									Next: nil,
								},
							},
						},
					},
				},
			},
		}, 6)
	}
}

func Benchmark_removeElementsRecursiveMutable(b *testing.B) {
	for i := 0; i < b.N; i++ {
		removeElementsRecursiveMutable(&ListNode{
			Val: 1,
			Next: &ListNode{
				Val: 2,
				Next: &ListNode{
					Val: 6,
					Next: &ListNode{
						Val: 3,
						Next: &ListNode{
							Val: 4,
							Next: &ListNode{
								Val: 5,
								Next: &ListNode{
									Val:  6,
									Next: nil,
								},
							},
						},
					},
				},
			},
		}, 6)
	}
}
