package p203_remove_linked_list_elements

type ListNode struct {
	Val  int
	Next *ListNode
}

// Keeps original list intact
// Creates a new list for result, consuming additional memory
func removeElementsRecursiveImmutable(head *ListNode, val int) *ListNode {
	if head == nil {
		return nil
	}
	node := &ListNode{
		Val:  head.Val,
		Next: removeElementsRecursiveImmutable(head.Next, val),
	}
	if head.Val == val {
		return node.Next
	}
	return node
}

// Mutates the inputh list messing it up
// Doesn't use additional memory
// Update: doesn't seem to save any memory acording to benchmarks
func removeElementsRecursiveMutable(head *ListNode, val int) *ListNode {
	if head == nil {
		return nil
	}
	head.Next = removeElementsRecursiveMutable(head.Next, val)
	if head.Val == val {
		return head.Next
	}
	return head
}
