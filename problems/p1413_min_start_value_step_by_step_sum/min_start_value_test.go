package p1413_min_start_value_step_by_step_sum

import (
	"testing"
)

func Test_minStartValue(t *testing.T) {
	tests := []struct {
		name string
		nums []int
		want int
	}{
		{
			name: "goes below zero",
			nums: []int{-3, 2, -3, 4, 2},
			want: 5,
		},
		{
			name: "all positive",
			nums: []int{3, 2},
			want: 1,
		},
		{
			name: "single num negative",
			nums: []int{-2},
			want: 3,
		},
		{
			name: "single num positive",
			nums: []int{2},
			want: 1,
		},
		{
			name: "empty",
			nums: []int{},
			want: 1,
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := minStartValue(tt.nums); got != tt.want {
				t.Errorf("minStartValue() = %v, want %v", got, tt.want)
			}
		})
	}
}

func Benchmark_minStartValue(b *testing.B) {
	for i := 0; i < b.N; i++ {
		minStartValue([]int{-3, 2, -3, 4, 2, 6, -4, 9, -10, 1})
	}
}
