package p1413_min_start_value_step_by_step_sum

func minStartValue(nums []int) int {
	sum := 0
	min := 0
	for _, n := range nums {
		sum += n
		if sum < min {
			min = sum
		}
	}
	if min > 0 {
		return 1
	}
	return 1 - min
}
