# 344. [Reverse String](https://leetcode.com/problems/reverse-string/)

Write a function that reverses a string. The input string is given as an array of characters `s`.

## Constraints

* `1 <= s.length <= 105`
* `s[i]` is a printable ascii character.

## Solutions

For recursive solution see `learn/algorithms/recursion_1/reverse_string`
