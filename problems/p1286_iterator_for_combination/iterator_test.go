package p1286_iterator_for_combination

import (
	"reflect"
	"testing"
)

func TestCombinationIterator(t *testing.T) {
	tests := []struct {
		name              string
		characters        string
		combinationLength int
		want              []string
	}{
		{
			name:              "1 from 4",
			characters:        "abcd",
			combinationLength: 1,
			want:              []string{"a", "b", "c", "d"},
		},
		{
			name:              "2 from 4",
			characters:        "abcd",
			combinationLength: 2,
			want:              []string{"ab", "ac", "ad", "bc", "bd", "cd"},
		},
		{
			name:              "3 from 4",
			characters:        "abcd",
			combinationLength: 3,
			want:              []string{"abc", "abd", "acd", "bcd"},
		},
		{
			name:              "4 from 4",
			characters:        "abcd",
			combinationLength: 4,
			want:              []string{"abcd"},
		},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			i := Constructor(tt.characters, tt.combinationLength)
			got := make([]string, 0, len(tt.want))
			max := 1 << len(tt.characters)
			for len(got) <= max {
				got = append(got, i.Next())
				if !i.HasNext() {
					break
				}
			}
			if len(got) > len(tt.want) {
				t.Errorf("Getting to much combinations, probable infinite loop")
			}
			if !reflect.DeepEqual(got, tt.want) {
				t.Errorf("Got %v, want %v", got, tt.want)
			}
		})
	}
}

func BenchmarkCombinationIterator(b *testing.B) {
	b.Run("1 of 10", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghij", 1)
			for it.HasNext() {
				it.Next()
			}
		}
	})
	b.Run("4 of 10", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghij", 4)
			for it.HasNext() {
				it.Next()
			}
		}
	})
	b.Run("10 of 10", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghij", 10)
			for it.HasNext() {
				it.Next()
			}
		}
	})
	b.Run("1 of 15", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghijklmno", 1)
			for it.HasNext() {
				it.Next()
			}
		}
	})
	b.Run("4 of 15", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghijklmno", 4)
			for it.HasNext() {
				it.Next()
			}
		}
	})
	b.Run("15 of 15", func(b *testing.B) {
		for i := 0; i < b.N; i++ {
			it := Constructor("abcdefghijklmno", 15)
			for it.HasNext() {
				it.Next()
			}
		}
	})
}
