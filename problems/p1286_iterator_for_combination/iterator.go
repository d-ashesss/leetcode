package p1286_iterator_for_combination

/**
 * Your CombinationIterator object will be instantiated and called as such:
 * obj := Constructor(characters, combinationLength);
 * param_1 := obj.Next();
 * param_2 := obj.HasNext();
 */

type CombinationIterator struct {
	combinations []string
}

func Constructor(characters string, combinationLength int) CombinationIterator {
	maxCombinations := 1 << len(characters)
	combinations := make([]string, 0, maxCombinations)
	for i := maxCombinations - 1; i > 0; i-- {
		matches := 0
		cur := i
		for cur > 0 {
			if cur&1 > 0 {
				matches++
			}
			cur >>= 1
		}
		if matches == combinationLength {
			combination := ""
			cur := i
			for j := len(characters) - 1; j >= 0; j-- {
				if cur&1 == 1 {
					combination = string(characters[j]) + combination
				}
				cur = cur >> 1
			}
			combinations = append(combinations, combination)
		}
	}
	return CombinationIterator{
		combinations: combinations,
	}
}

func (i *CombinationIterator) Next() string {
	n := i.combinations[0]
	i.combinations = i.combinations[1:]
	return n
}

func (i CombinationIterator) HasNext() bool {
	return len(i.combinations) > 0
}
